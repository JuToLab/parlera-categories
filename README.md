# Categories for Parlera

## Downloadable categories
You can download these categories and import them into [Parlera](https://gitlab.com/enjoyingfoss/parlera):

- [Indie movies](https://gitlab.com/enjoyingfoss/parlera-categories/-/raw/main/categories/en/Indie_movies.parlera?inline=false)

## Contributing
If you'd like to contribute your own category, file a merge request or [create a new issue](https://gitlab.com/enjoyingfoss/parlera-categories/-/issues/new) and attach the exported Parlera file.

All contributions must be strictly your own work and licensed under the CC BY-SA 4.0 license. To declare a contribution as such, use this sentence in your contribution:

> I hereby declare that this work is completely my own and agree to release the contributions attached in this comment under the CC BY-SA 4.0 license.

The words you use may not be copied from any third-party source.

## License
All categories in this repository are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/).
